# github-private-new

A Chrome Extension which changes default of "New Repository" to "Private" in GitHub

![demo1](demo_images/demo1.gif)
